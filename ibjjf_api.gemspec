# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ibjjf_rankings_api/version'

Gem::Specification.new do |spec|
  spec.name          = "ibjjf_rankings_api"
  spec.version       = IbjjfRankingsApi::VERSION
  spec.authors       = ["Vitor HP Bittencourt"]
  spec.email         = ["vitorhp2@gmail.com"]

  spec.summary       = "Wraps Ranking REST API"
  spec.description   = "Wraps Ranking REST API"
  spec.homepage      = ""
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", '~> 2'
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "pry", "~> 0.10.3"
  spec.add_development_dependency "guard", "~> 2.14"
  spec.add_development_dependency 'guard-rspec', '~> 4.7'

  spec.add_runtime_dependency "retryable", "~> 3.0.0"
  spec.add_runtime_dependency "rest-client", "~> 2.0.2"
  spec.add_runtime_dependency 'addressable', '~> 2.3.0'
end
