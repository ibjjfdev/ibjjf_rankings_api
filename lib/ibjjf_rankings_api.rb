require "ibjjf_rankings_api/version"

module IbjjfRankingsApi
  autoload :ApiError, 'ibjjf_rankings_api/api_error'
  autoload :ForbiddenError, 'ibjjf_rankings_api/forbidden_error'
  autoload :NotFoundError, 'ibjjf_rankings_api/not_found_error'

  autoload :Client, 'ibjjf_rankings_api/client'
end
