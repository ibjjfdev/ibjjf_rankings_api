# Pegar token:

# POST https://ranking-staging.herokuapp.com/api/v1/sessions/authenticate
# { "client_id": "19D2C586-4DF9-4694-91FD-925434755BFD",
#   "client_secret": "028b36b97ac507a0638ad5557f0b1a1f" }

# GET https://ranking-staging.herokuapp.com/api/v1/championships/championship_id/brackets?category_id=category_id
# Retorna a lista de atletas priorizados na categoria filtrada

# GET https://ranking-staging.herokuapp.com/api/v1/championships/championship_id/weight
# Retorna o peso definido para o campeonato

# GET https://ranking-staging.herokuapp.com/api/v1/rankings/athletes_results?ranking_slug=slug&categories_key='male_adult_black'
# Retorna a lista dos resultados dos atletas para um o ranking filtrado
# Pode ser filtrado por categories key, ex: "gender_age-division_belt"

# GET https://ranking-staging.herokuapp.com/api/v1/rankings/academies_results?ranking_slug=slug
# Retorna a lista dos resultados das academias de acordo com o slug do ranking

# GET https://ranking-staging.herokuapp.com/api/v1/rankings/athlete_rankings?athlete_slug=athlete_slug
# Retorna as posições de um atleta nos rankings em que o mesmo aparece, bem como os eventos que compuseram os resultados

require 'retryable'
require 'json'
require 'rest-client'
require 'addressable/uri'

module IbjjfRankingsApi
  class Client
    PAGE_SIZE = 500
    MAX_TRIES = 5

    attr_reader :auth_token, :client_id, :client_secret

    def initialize host_url: nil, client_id: nil, client_secret: nil, api_version: "v1"
      @api_version = api_version || ENV['IBJJF_RANKINGS_API_VERSION']
      @host_url = "#{host_url || ENV['IBJJF_RANKINGS_API_HOST']}/api/#{@api_version}"
      @client_id = client_id || ENV['IBJJF_RANKINGS_API_CLIENT_ID']
      @client_secret = client_secret || ENV['IBJJF_RANKINGS_API_CLIENT_SECRET']
    end

    def brackets(championship_id, category_id)
      get "#{@host_url}/championships/#{championship_id}/brackets?category_id=#{category_id}", {}
    end

    def championship_weight(championship_id)
      get "#{@host_url}/championships/#{championship_id}/weight", {}
    end

    def last_publication_version(params)
      get "#{@host_url}/rankings/last_publication_version", params
    end

    def athlete_results(params)
      get "#{@host_url}/rankings/athlete_results", params
    end

    def academy_results(params)
      get "#{@host_url}/rankings/academy_results", params
    end

    def overall_seasons
      get "#{@host_url}/rankings/overall_seasons"
    end

    def athlete_rankings(athlete_slug)
      get "#{@host_url}/rankings/athlete_rankings?athlete_slug=#{athlete_slug}"
    end

    def prioritized_championship(championship_id)
      get "#{@host_url}/championships/#{championship_id}/prioritized_championship", {}
    end

    def authenticate!
      @auth_token = parse_response(
        ::RestClient.post(
          "#{@host_url}/sessions/authenticate",
          {
            client_id: client_id || ENV['IBJJF_RANKINGS_API_CLIENT_ID'],
            client_secret: client_secret || ENV['IBJJF_RANKINGS_API_CLIENT_SECRET']
          }.to_json,
          {
            content_type: :json,
            accept: :json
          }
        )
      )[:token]
    end

    private

    def get(endpoint, params = {})
      endpoint += "?#{assembly_query(params)}" unless params.empty?
      call_api(:get, [endpoint])
    end

    def post(endpoint, payload)
      call_api(:post, [endpoint, payload.to_json])
    end

    def delete(endpoint)
      call_api(:delete, [endpoint])
    end

    def call_api(method, params)
      authenticate! if @auth_token.nil?

      p = params.push(default_headers.merge(authorization_header))

      begin
        retries ||= 0

        ::RestClient.send(method, *p) do |response, request|

          case response.code
            when 200
              parse_response(response)
            when 502, 500, 504
              Rails.logger.tagged("IBJJF::Client##{method}") do
                Rails.logger.info 'Invalid unhandled API response'
                Rails.logger.info "Request: #{request.uri}"
                Rails.logger.info "Response code: #{response.code}"
                Rails.logger.info "Response body: #{response.body}"
                Rails.logger.info "Params: #{params}"
              end if defined?(Rails)

              raise IbjjfRankingsApi::ApiError.new(response.code, 'Internal server error')
            when 403
              raise IbjjfRankingsApi::ForbiddenError.new
            when 404
              raise IbjjfRankingsApi::NotFoundError.new(response.code, 'Not found')

              # raise 'IBJJF::Client - Invalid unhandled API response'
          end
        end
      rescue IbjjfRankingsApi::ForbiddenError
        authenticate!

        retry if (retries += 1) <= 1

        raise
      end
    end

    def parse_response(response)
      unless response.body.empty?
        begin
          JSON.parse(response.body, symbolize_names: true)
        rescue JSON::ParserError
          response.body
        end
      end
    end

    def default_headers
      {
        content_type: :json,
        accept: :json
      }
    end

    def authorization_header
      {
        authorization: "Bearer #{@auth_token}"
      }
    end

    def assembly_query(params)
      uri = Addressable::URI.new
      uri.query_values = params
      uri.query
    end
  end

end
