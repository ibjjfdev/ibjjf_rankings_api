module IbjjfRankingsApi
  class NotFoundError < StandardError
    attr_reader :code, :message

    def initialize(code, message)
      @code = code
      @message = message
    end

    def to_s
      "[HTTP #{code}] #{message}"
    end
  end
end
