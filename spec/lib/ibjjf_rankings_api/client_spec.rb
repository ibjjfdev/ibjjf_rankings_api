require 'spec_helper'
require 'pry'

describe IbjjfRankingsApi::Client do
  subject do
    IbjjfRankingsApi::Client.new(
      host_url: 'https://ranking-staging.herokuapp.com',
      api_version: 'v1',
      client_id: '1297A370-6B2B-4E51-A449-1D8BA99166F6',
      client_secret: 'PjNDe8ETzIskJqhD1G4h1jftRwQmz81n1cKteUaDtGA='
    )
  end

  let(:category_id) { 1_505_573 }
  let(:tournament_id) { 1209 }
  let(:athlete_slug) { 'claudia-doval' }

  describe 'GET #brackets' do
    let(:response) { subject.brackets(tournament_id, category_id) }

    it 'is an object' do
      expect(response.first).to be_a Hash
    end
  end

  describe 'GET #championship_weight' do
    let(:response) { subject.championship_weight(899) }

    it 'is an object' do
      expect(response).to be_a Hash
    end

    it 'has the championship_weight field' do
      expect(response).to have_key :championship_weight
    end
  end

  describe 'GET #athlete_results' do
    let(:default_params) do
      {
        ranking_slug: 'ranking-geral-gi',
        categories_key: 'male_adult_black'
      }
    end

    let(:response) { subject.athlete_results(default_params) }

    it 'is an object' do
      expect(response).to be_a Hash
    end

    it 'has a list of specific fields' do
      %i[year result last_update].each do |field|
        expect(response).to have_key field
      end
    end

    it 'the field result is an object' do
      expect(response[:result].first).to be_a Hash
    end

    it 'the athlete result has a list of specific fields' do
      athlete_result_sample = response[:result].first

      fields = %i[academy name photo id country_code points position old_position]

      fields.each do |field|
        expect(athlete_result_sample).to have_key field
      end
    end
  end

  describe 'GET #academy_results' do
    let(:default_params) do
      {
        ranking_slug: 'ranking-academias-overall'
      }
    end

    let(:response) { subject.academy_results(default_params) }

    it 'is an object' do
      expect(response).to be_a Hash
    end

    it 'has a list of specific fields' do
      %i[year result last_update].each do |field|
        expect(response).to have_key field
      end
    end

    it 'the field result is an object' do
      expect(response[:result].first).to be_a Hash
    end

    it 'the academy result has a list of specific fields' do
      academy_result_sample = response[:result].first

      fields = %i[position academy_name total_points score_history]

      fields.each do |field|
        expect(academy_result_sample).to have_key field
      end
    end
  end

  describe 'GET #last_publication_version' do
    let(:params) { { ranking_slug: 'ranking-geral-gi' } }
    let(:response) { subject.last_publication_version(params) }

    it 'is an object' do
      expect(response).to be_a Hash
    end

    it 'has a version field' do
      expect(response).to have_key :version
    end
  end

  describe 'GET #overall_seasons' do
    let(:response) { subject.overall_seasons }
    let(:ranking_names) do
      %i[
        ranking-academias-grandslam
        ranking-academias-overall
        ranking-geral-gi
        ranking-masters
        ranking-geral-no-gi
        ranking-geral-gi-cbjj
        ranking-grand-slam-gi
        ranking-grand-slam-master
        ranking-grand-slam-no-gi
      ]
    end

    it 'is an object' do
      expect(response).to be_a Hash
    end

    it 'ranking name is an array of objects' do
      ranking_names.each { |ranking| expect(response[ranking]).to be_a Array }
    end
  end

  describe 'GET #athlete_rankings' do
    let(:response) { subject.athlete_rankings(athlete_slug) }

    it 'is an object' do
      expect(response).to be_a Hash
    end

    it 'the response has a list of specific fields' do
      fields = %i[name photo belt academy ranking_data]

      fields.each do |field|
        expect(response).to have_key field
      end
    end
  end

  describe 'GET #prioritized_championship' do
    let(:response) { subject.prioritized_championship(tournament_id) }

    it 'is an object' do
      expect(response).to be_a Hash
    end

    it 'the response has the field championship_priorization' do
      expect(response).to have_key(:championship_priorization)
    end
  end
end
